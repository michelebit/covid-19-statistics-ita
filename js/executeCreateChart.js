// Create chart information
var executeCreateChart = (xAxisData, yAxisData, label) => {
    var chart = {};
    chart.labels = xAxisData; // Axis x data
    chart.series = [label]; // Chart label
    chart.data = [yAxisData]; // Axis x data
    return chart;
}