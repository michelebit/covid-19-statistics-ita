// Chart options
var executeGetChartsOptions = () => {
    var options = {
        scales: {
            yAxes: [
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks : {
                        suggestedMax: 15
                    }
                }
            ],
            /* Design max 5 ticks
            xAxes: [{
                ticks: {
                    autoSkip: true,
                    maxTicksLimit: 5
                }
            }],
            */
        },
        animation: {
            duration: 0 // general animation time
        },
        hover: {
            animationDuration: 0 // duration of animations when hovering an item
        },
        responsiveAnimationDuration: 0, // animation duration after a resize
        elements: { point: { radius: 0.3 } }
    };
    return options;
}