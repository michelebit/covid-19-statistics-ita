// Create charts information with two y axis
executeCreateChartPlus = (xAxisData, yAxisData, labels) => {
    var chart = {};
    chart.labels = xAxisData;
    chart.series = labels;
    chart.data = yAxisData;
    return chart;
}