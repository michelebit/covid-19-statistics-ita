// Get chart colors
var executeGetColors = () => {
    var colors = {
        red: ['#e00157'],
        green: ['#01e050'],
        yellow: ['#a39700'],
        multiple: ['#e00157', '#01e050', '#0a005a'],
    };

    return colors;
}