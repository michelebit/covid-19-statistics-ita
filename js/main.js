var app = angular.module('covidApp', ["chart.js", 'ngMaterial', 'ngMessages', 'ngRoute']);

var path = {
    root: "/",
    worldData: "/worldData"
}

app.config(function ($routeProvider) {
    $routeProvider.when(path.root, {
        templateUrl: 'html/mainBody/nationalTrend/view.html',
        controller: 'nationalTrendCtrl'
    }).when(path.worldData, {
        templateUrl: 'html/mainBody/worldTrend/view.html',
        controller: 'worldTrendCtrl'
    }).otherwise({
        redirectTo: path.root
    });
});

app.controller('mainCtrl', function ($rootScope, $location) {
    $rootScope.darkTheme = true;
    $rootScope.shareButtons = false;
    $rootScope.showBuyMeCoffee = false;
    $rootScope.isWorldData = $location.path() == path.worldData;
    $rootScope.nationalTrend = {};
    $rootScope.worldTrend = {};

    // $ROOTSCOPE FUNCTIONS
    $rootScope.executeDarkTheme = (isDark) => {
        $rootScope.darkTheme = isDark;
    };

    $rootScope.executeShowBuyMeCoffe = (isShowBuyMeCoffee) => {
        $rootScope.showBuyMeCoffee = isShowBuyMeCoffee;
    };

    $rootScope.setShareButtons = () => {
        $rootScope.shareButtons = !$rootScope.shareButtons;
    }

    $rootScope.changeRoute = function(path){
        $location.path(path);
        $rootScope.isWorldData = !$rootScope.isWorldData;
    };
});

