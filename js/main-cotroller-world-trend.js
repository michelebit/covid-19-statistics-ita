angular.module('covidApp').controller("worldTrendCtrl", function ($scope, $http, $rootScope) {
    const URL_WORLD_DATA = "https://corona.lmao.ninja/v2/countries?sort=cases";
    const LOAD_INDEX = 20;

    $scope.loading = true;
    $scope.callError = false;

    var currLoadIndex = 0; // 0 is World Data
    allDataWorld = null;
    $scope.isLoadMore = true;
    $scope.searchedNation = "";
    $scope.isSearchNation = false;

    $scope.loadDataWorld = (reset) => {
        if(reset) {
            currLoadIndex = 0;
            $rootScope.worldTrend.dataWorld = [];
        }
        var nextLoadIndex = currLoadIndex + LOAD_INDEX;
        var newData = allDataWorld.slice(currLoadIndex, nextLoadIndex);
        $rootScope.worldTrend.dataWorld = $rootScope.worldTrend.dataWorld.concat(newData);
        currLoadIndex = nextLoadIndex;
        if($rootScope.worldTrend.dataWorld.length === allDataWorld.length) {
            $scope.isLoadMore = false;
        }
    }

    $scope.searchNation = (value) => {
        if(value.length <= 1) {
            $scope.loadDataWorld(true);
            $scope.isSearchNation = false;
            return;
        }
        $scope.isSearchNation = true;
        $rootScope.worldTrend.dataWorld = allDataWorld.filter((el) => el.country.toLowerCase().includes(value.toLowerCase()));
    }

    // GET JSON WORLD DATA
    $http.get(URL_WORLD_DATA).then((responseWorld) => {
        if (responseWorld.status == 200) {
            allDataWorld = responseWorld.data;
            $rootScope.worldTrend.dataWorld = [];
            $scope.loadDataWorld(false);
            $scope.loading = false;
        }
        else {
            $scope.callError = true;
        }
    }, function errorCallback() {
        $scope.callError = true;
    });

});