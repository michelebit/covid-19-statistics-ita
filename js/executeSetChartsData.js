var executeSetChartsData = (data,
    date,
    positive,
    intensiveCare,
    resigned,
    deceased,
    newPositive,
    percNewPositiveSwabs,
    newResigned,
    newDeceased,
    newIntensiveCare,
    newSwabs) => { 
        data.map(((el, index, array) => {
            
            date.push(el.data.slice(0, 10));

            positive.push(el.totale_positivi);

            intensiveCare.push(el.terapia_intensiva);

            resigned.push(el.dimessi_guariti);

            deceased.push(el.deceduti);

            newPositive.push(el.nuovi_positivi);

            // Calculation new Resigned - Greater than 0
            var resignedCurr = el.dimessi_guariti;
            var resignedPrev = array[index - 1] ? array[index - 1].dimessi_guariti : 0;
            var resignedDiff = resignedCurr - resignedPrev;
            newResigned.push(resignedDiff);

            // Calculation new Deceased - Greater than 0
            var deceasedCurr = el.deceduti;
            var deceasedPrev = array[index - 1] ? array[index - 1].deceduti : 0;
            var deceasedDiff = deceasedCurr - deceasedPrev;
            newDeceased.push(deceasedDiff);

            // Calculation new Swabs - Greater than 0
            var swabsCurr = el.tamponi;
            var swabsPrev = array[index - 1] ? array[index - 1].tamponi : 0;
            var swabsDiff = swabsCurr - swabsPrev;
            newSwabs.push(swabsDiff);


            // Calculation new IntensiveCare
            var intensiveCareCurr = el.terapia_intensiva;
            var intensiveCarePrev = array[index - 1] ? array[index - 1].terapia_intensiva : 0;
            var intensiveCareDiff = intensiveCareCurr - intensiveCarePrev;
            newIntensiveCare.push(intensiveCareDiff);

            // Calculation New Positive / Swabs
            var diffSwabs = array[index - 1] ? el.tamponi - array[index - 1].tamponi : 0;
            var percentNewPositiveSwabs = diffSwabs===0 ? 0 : ((el.nuovi_positivi / diffSwabs) * 100).toFixed(1);
            percNewPositiveSwabs.push(percentNewPositiveSwabs);
        }
    ));
}