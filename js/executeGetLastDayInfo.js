// Last day information
var executeGetLastDayInfo = (trend) => {
    length = trend.length;
    var lastDay = trend[length - 1];
    var yesterday = trend[length - 2];
    var percIntensiveCare = (lastDay.terapia_intensiva / lastDay.totale_positivi) * 100;
    var percDeceased = (lastDay.deceduti / lastDay.totale_casi) * 100;
    var newDeceased = lastDay.deceduti - yesterday.deceduti;
    var newResigned = lastDay.dimessi_guariti - yesterday.dimessi_guariti;
    var newSwabs = lastDay.tamponi - yesterday.tamponi;

    var date = executeGetDataString(lastDay.data);

    var lastDayInfo = {
        date: date,
        info: [
            { title: "Nuovi Positivi", desc: "+"+lastDay.nuovi_positivi },
            { title: "Attualmente Positivi", desc: lastDay.totale_positivi },
            { title: "Nuovi Deceduti", desc: "+"+newDeceased },
            { title: "Nuovi Tamponi", desc: "+"+newSwabs, yellow: true },
            { title: "Terapia Intensiva", desc: lastDay.terapia_intensiva + " (" + percIntensiveCare.toFixed(1) + "%)" },
            { title: "Nuovi Pos. / Tamponi", desc: ((lastDay.nuovi_positivi / newSwabs) * 100).toFixed(1) + "%" },
            { title: "Nuovi Guariti", desc: "+"+newResigned, green: true },
            { title: "Contagi Totali", desc: lastDay.totale_casi },
            { title: "Deceduti Totali", desc: lastDay.deceduti + " (" + percDeceased.toFixed(1) + "%)" },
            { title: "Tamponi Totali", desc: lastDay.tamponi },
            ]
    };
    
    return lastDayInfo;
} 