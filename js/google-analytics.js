window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());

gtag('config', 'UA-160749443-1');
gtag('config', 'UA-160749443-2');
gtag('config', 'UA-160749443-3');