angular.module('covidApp').controller('nationalTrendCtrl', function ($scope, $rootScope, $http) {
    // INIT CONST
    const URL_DATA_NATION = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json";
    const URL_DATA_REGION = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json";

    const DATA_SET_ANDAMENTO_NAZIONALE = "andamento_nazionale";

    const labelChart = {
        positive: "Attualmente Positivi",
        intensiveCare: "Terapia Intensiva",
        percNewPositiveSwabs: "% Nuovi Positivi/Tamponi ",
        resigned: "Guariti",
        deceased: "Deceduti",
        newPositive: "Nuovi Positivi",
        newResigned: "Nuovi Guariti",
        newDeceased: "Nuovi Deceduti",
        newIntensiveCare: "Incremento Terapie Intensive",
        newSwabs: "Tamponi Giornalieri"
    };

    // INIT VAR (Charts Data)
    var date = [];
    var positive = [];
    var intensiveCare = [];
    var newPositive = [];
    var newResigned = [];
    var percNewPositiveSwabs = [];
    var resigned = [];
    var deceased = [];
    var newDeceased = [];
    var newIntensiveCare = [];
    var newSwabs = [];


    // INIT SCOPE VAR
    $scope.loading = true;
    $scope.callError = false;

    $scope.NATIONAL_AREA = "Nazionale";
    $scope.currentArea = $scope.NATIONAL_AREA;
    $scope.geographicArea = [$scope.NATIONAL_AREA, "Abruzzo", "Basilicata", "Calabria", "Campania", "Emilia-Romagna", "Friuli Venezia Giulia", "Lazio", "Liguria", "Lombardia", "Marche", "Molise", "Piemonte", "Puglia", "Sardegna", "Sicilia", "Toscana", "P.A. Bolzano", "P.A. Trento", "Umbria", "Valle d'Aosta", "Veneto"];
    $scope.chartsPeriod = [{days: "Tutto", value: 0}, {days: "Ultima Settimana", value: -8}, {days: "Ultimo Mese", value: -31}, {days: "Ultimi Tre Mesi", value: -93}];
    $scope.currentPeriod = $scope.chartsPeriod[2].value;

    // CHARTS OPTIONS
    $scope.datasetOverride = executeGetDataSetOverride();
    $scope.colors = executeGetColors();
    $scope.options = executeGetChartsOptions();

    // $SCOPE FUNCTIONS
    $scope.executeCreateCharts = (area, daysCharts) => {
        date = [];
        positive = [];
        intensiveCare = [];
        newPositive = [];
        newResigned = [];
        percNewPositiveSwabs = [];
        resigned = [];
        deceased = [];
        newDeceased = [];
        newIntensiveCare = [];
        newSwabs = [];

        var arrArea = [];

        // Get area data
        arrArea = area === $scope.NATIONAL_AREA ? $rootScope.nationalTrend.dataNation : $rootScope.nationalTrend.dataRegion.filter((el) => area == el.denominazione_regione);

        if(arrArea.length == 0 || arrArea == null)
            return;

        // Set Charts Data
        executeSetChartsData(arrArea, date, positive, intensiveCare, resigned, deceased, newPositive, percNewPositiveSwabs, newResigned, newDeceased, newIntensiveCare, newSwabs);

        $scope.currentArea = area;
        $scope.currentPeriod = daysCharts;
        // Last day info
        $scope.lastDayInfo = executeGetLastDayInfo(arrArea);

        // daysCharts == 0 -> use all data
        // Create Charts
        $scope.chartPositive = executeCreateChart(date.slice(daysCharts), positive.slice(daysCharts), labelChart.positive);
        $scope.chartIntensiveCare = executeCreateChart(date.slice(daysCharts), intensiveCare.slice(daysCharts), labelChart.intensiveCare);
        $scope.chartPercNewPositiveSwabs = executeCreateChart(date.slice(daysCharts), percNewPositiveSwabs.slice(daysCharts), labelChart.percNewPositiveSwabs);
        $scope.chartResigned = executeCreateChart(date.slice(daysCharts), resigned.slice(daysCharts), labelChart.resigned);
        $scope.chartDeceased = executeCreateChart(date.slice(daysCharts), deceased.slice(daysCharts), labelChart.deceased);
        $scope.chartNewDeceased = executeCreateChart(date.slice(daysCharts), newDeceased.slice(daysCharts), labelChart.newDeceased);
        $scope.chartNewIntensiveCare = executeCreateChart(date.slice(daysCharts), newIntensiveCare.slice(daysCharts), labelChart.newIntensiveCare);
        $scope.chartNewSwabs = executeCreateChart(date.slice(daysCharts), newSwabs.slice(daysCharts), labelChart.newSwabs);
        $scope.chartNewPositiveResigned = executeCreateChartPlus(date.slice(daysCharts), [newPositive.slice(daysCharts), newResigned.slice(daysCharts)], [labelChart.newPositive, labelChart.newResigned]);
    }

    $scope.executeCreateNote = (data) => {
        $rootScope.nationalTrend.dataNote = [];
        data.filter(el => el.dataset === DATA_SET_ANDAMENTO_NAZIONALE)
            .map((el) => {
                var data = executeGetDataString(el.data);
                $rootScope.nationalTrend.dataNote.push(
                {
                    date: data.slice(0, data.indexOf(',')),
                    region: el.regione,
                    note: el.tipologia_avviso,
                    provincia: el.provincia
                }
            )}
        );

    }

    // If data are into rootScope -> don't reload all data with http get
    if($rootScope.nationalTrend.dataNation && $rootScope.nationalTrend.dataRegion && $rootScope.nationalTrend.dataNote) {
        $scope.executeCreateCharts($scope.NATIONAL_AREA, $scope.currentPeriod);
        $scope.loading = false;
    } else {
        // GET JSON NATIONAL DATA
        $http.get(URL_DATA_NATION).then((responseNation) => {
            if (responseNation.status == 200) {
                // GET JSON REGIONAL DATA
                $http.get(URL_DATA_REGION).then((responseRegion) => {
                    if (responseRegion.status == 200) {
                        $rootScope.nationalTrend.dataRegion = responseRegion.data;
                        $rootScope.nationalTrend.dataNation = responseNation.data;
                        $scope.executeCreateCharts($scope.NATIONAL_AREA, $scope.currentPeriod);
                        $scope.loading = false;
                    }
                    else {
                        $scope.callError = true;
                    }
                }, function errorCallback() {
                    $scope.callError = true;
                });
            }
            else {
                $scope.callError = true;
            }
        }, function errorCallback() {
            $scope.callError = true;
        });
    }
});